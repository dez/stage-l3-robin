\documentclass{beamer}

\input{./preambule/preambule-beamer.tex}

\usepackage{tikz-cats}

\usepackage{bbold}


\author{Robin {Jourde}}
\title[Soutenance de stage de L3]{Soutenance de stage de L3 :\\ Sémantique opéradique de langages linéaires}
\date[31/05/21 -- 9/06/21]{31 mai 2021 -- 9 juilllet 2021}

%% macros utiles dans cet article
\newcommand{\PSet}{[\P, \Set]}
\renewcommand{\Z}{\varmathbb Z}
\newcommand{\Zmod}{\cattxt{\Z}{mod}}

\newcommand\SP{[S, \P]}
\newcommand\SSet{[S, \Set]}
\newcommand\SPSSet{[\SP, \SSet]}

\newcommand\ZSmod{\cattxt{\Z_S}{mod}}

\newcommand\T{\mathbb T}
\newcommand\LL{\mathbb L}

\DeclareMathOperator*\sep{\#}



\begin{document}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}{Table de mati\`{e}res}
  \tableofcontents
\end{frame}

\section{Objectifs et motivation}
\label{sec:motiv}
\begin{frame}{Objectifs et motivation}

  \begin{itemize}
  \item<+-> modéliser des langages de programmation
  \item<+-> langages linéaires
  \item<+-> théorie des catégories
  \item<+-> théorie générale et abstraite
  \end{itemize}

\end{frame}

\section{Syntaxe et signature liante}
\label{sec:sig}
\begin{frame}{Syntaxe et signature liante}

  \begin{itemize}
  \item<+-> syntaxe abstraite
  \item<+-> \emph{signature} pour décrire un langage
  \item<+-> modéliser les signatures
  \end{itemize}

  \begin{ex}<+->[$\lambda$-calcul linéaire]
    2 opérations : $\lambda$-abstraction et l'application
    \begin{center}
      \begin{tabular}{ccc}
        \AxiomC{}
        \UnaryInfC{$1 \vdash x_1$}
        \DisplayProof
        &
          \RightLabel{$\lambda$}
          \AxiomC{$n+1 \vdash t$}
          \UnaryInfC{$n \vdash \lambda x_{n+1}. t$}
          \DisplayProof
        &
          \RightLabel{$App$}
          \AxiomC{$n \vdash t$}
          \AxiomC{$m \vdash u$}
          \BinaryInfC{$n+m \vdash t'\;u'$}
          \DisplayProof
      \end{tabular}
    \end{center}
  \end{ex}

\end{frame}
\subsection{Arité}
\label{sec:ar}
\begin{frame}{Arité}

  \begin{itemize}
  \item<+-> \emph{arité} d'une opération : $k$-uplet d'entiers $\langle n_1 \dots n_k \rangle$
    \begin{itemize}
    \item<+-> $k$ arguments
    \item<+-> $n_i$ variables liées dans le $i$-ème argument
    \end{itemize}
  \end{itemize}

  \begin{ex}<+->[$\lambda$-calcul linéaire]
    \begin{description}
    \item[$\lambda$-abstraction] $\langle 1 \rangle$
    \item[application] $\langle 0,0 \rangle$
    \end{description}
  \end{ex}

  \begin{ex}<+->[$letrec_n$]
    $letrec_n(e_1, \dots, e_n,e) := \texttt{let rec } x_1 = e_1 \texttt{ and } \dots \texttt{ and } x_n = e_n \texttt{ in } e$

    arité : $\langle 0, \dots, 0, n\rangle$
  \end{ex}

\end{frame}
\subsection{Syntaxe}
\label{sec:synt}
\begin{frame}{Syntaxe}

  \begin{itemize}
  \item<+-> engendrer la syntaxe, les termes
  \item<+-> opérateur $\Sigma$ à partir des arités
  \item<+-> plus petit point fixe : \emph{$\Sigma$-algèbre initiale}
  \item<+-> compatibilité avec la \emph{substitution} : \emph{$\Sigma$-monoïde}
    $$o(t_1 \dots t_k) [\sigma] = o(t_1[\drond^{n_1}\sigma] \dots  t_k[\drond^{n_k}\sigma])$$
  \end{itemize}

  \begin{thm}<+->[Sémantique d'algèbre initiale]
    La $\Sigma$-algèbre initiale est un $\Sigma$-monoïde et est initiale parmi les $\Sigma$-monoïdes.
  \end{thm}

  \begin{ccl}<+->
    \begin{center}
      langage $\to$ signature $\to$ catégorie de modèles (algèbres) $\to$ objet initial
    \end{center}
    Par théorème $\implies$ substitution (monoïde)
  \end{ccl}

\end{frame}

\section{Réduction}
\label{sec:red}
\begin{frame}{Réduction}

  \begin{itemize}
  \item<+-> $\Z = \{ termes \}$ $\Sigma$-monoïde initial
  \item<+-> relation de réduction
  \item<+-> règles de réduction
  \end{itemize}
  \begin{ex}<+->[$\lambda$-calcul : réduction à gauche dans une application] ~
    \begin{prooftree}
      \RightLabel{$App_g$}
      \AxiomC{$A \to A'$}
      \UnaryInfC{$A\; B \to A'\; B$}
    \end{prooftree}
  \end{ex}
  \begin{itemize}
  \item<+-> \emph{système} de règles
  \item<+-> \emph{modèle} des règles : quels termes se réduisent
  \end{itemize}

\end{frame}
\subsection{Représentation des règles}
\label{sec:rpz-regles}
\begin{frame}{Représentation des règles}

  \begin{description}
  \item<+->[Règle] triplet $P\from V \to C$ (<< \emph{span} >>)
    \visible<+->{ \begin{description}
      \item[$P$] prémisses
      \item[$C$] conclusion
      \item[$V$] variables de termes
      \item[$\from$ et $\to$] morphismes de construction des prémisses et conclusion
      \end{description} }

    \begin{ex}<+->[Span pour $App_g$]
      \begin{center}
        \diag(.1,1){
        \Z^2 \# 1 \& \Z^2 \# \Z \& \Z^2 \\
        (A,A')    \& (A,A';B)   \& (A\; B, A'\; B)
      }{
        (m-1-2) edge (m-1-1)
        edge (m-1-3)
      }
      \end{center}
    \end{ex}

  \item<+->[Relation] multigraphe $R$

    \begin{center}
      \diag{%
        R            \\%
        \Z   %
      }{%
        (m-1-1) edge[bend right=20, labell={s}] (m-2-1)%
        edge[bend left =20, labelr={t}] (m-2-1)%
      }
      $\iff$
      \diag{%
        R \\%
         \Z^2%
      }{%
        (m-1-1) edge[labelr={\langle s, t\rangle}] (m-2-1)%
      }
    \end{center}

  \end{description}

\end{frame}
\subsection{Représentation des modèles}
\label{sec:rpz-modele}
\begin{frame}{Représentation des modèles}

  \begin{itemize}
  \item<+-> modèle $R$ d'une règle  $P\from V \to C$
    \begin{itemize}
    \item<+-> multigraphe
    \item<+-> << $P \subset R \implies C \in R$ >>
    \end{itemize}

   
    \begin{ex}<+->[$App_g$] ~
    \begin{center}
      \only<-4>{
        \diag{
          p(R) \& \Sigma^r R  \& R          \\
          P    \& V           \& C
        }{
          (m-1-1) edge[labell={p(\langle s,t\rangle)}] (m-2-1)
          (m-1-2) edge (m-1-1)
          edge (m-2-2)
          edge[dashed] (m-1-3)
          (m-2-2) edge (m-2-1)
          edge (m-2-3)
          (m-1-3) edge[labelr={\langle s,t\rangle}] (m-2-3)
        }
    }
    \only<+->{
      \diag{
        R \# 1     \& \Sigma^r R = R \# \Z  \& R    \\
        \Z^2 \# 1  \& \Z^2 \# \Z            \& \Z^2
      }{
        (m-1-1) edge[labell={p(\langle s,t\rangle)}] (m-2-1)
        (m-1-2) edge (m-1-1)
        edge (m-2-2)
        edge[dashed] (m-1-3)
        (m-2-2) edge (m-2-1)
        edge (m-2-3)
        (m-1-3) edge[labelr={\langle s,t\rangle}] (m-2-3)
      }
    }
    \end{center}
  \end{ex}

  \item<+-> opérateur $\Sigma^r$
  \item<+-> on cherche : le plus petit modèle de toutes les règles simultanément
  \end{itemize}

\end{frame}

\section{Linéaire}
\label{sec:lin}
\begin{frame}{Linéaire}

  \begin{itemize}
  \item<+-> Linéaire :
    \emph{Chaque variable du contexte apparait 1 et 1 seule fois dans un terme}
  \item<+-> $n \vdash t \iff x_1\dots x_n \vdash t$
  \item<+-> objectif final : linéaire-non-linéaire (mixte)
  \item<+-> $\simeq$ exemple : Rust et pointeurs
  \end{itemize}

  \begin{obj}<+->
    Adapter le travail précédent au cas linéaire
  \end{obj}

\end{frame}

\section{Catégoriquement}
\label{sec:tech}
\begin{frame}{Catégoriquement}

  \begin{center}
    \danger \danger

    La suite de cet exposé consiste en l'explication un peu plus en détails du travail mené lors de ce stage.

    À partir de maintenant, j'utiliserai le vocabulaire des catégories et je considèrerai les concepts élémentaires de la théorie des catégories comme acquis.

    \danger \danger
  \end{center}

\end{frame}

\subsection{Syntaxe abstraite}
\begin{frame}{Syntaxe abstraite}

  \begin{itemize}

  \item<+-> catégorie ambiante $\PSet$ : termes indexés par les contextes
    \begin{dfn}<+->[catégorie $\P$]
      \begin{description}
      \item[objets] entiers naturels
      \item[morphismes] bijections : $\Hom(n, m) = \text{Bij}(n,m) \quad ( \neq \0 \iff n= m)$
      \end{description}
    \end{dfn}

  \item<+-> dérivée
    $$(\drond X)(n) = X(n+1)$$

  \item<+-> tenseur séparant, tenseur de Day
    $$(X\# Y)(n) = \int^{n_1,n_2} X(n_1) \times Y(n_2) \times [n_1 + n_2, n]$$

  \end{itemize}

\end{frame}
\begin{frame}

  \begin{itemize}

  \item<+-> signature
    $$\Sigma X = \coprod_{\langle n_i \rangle_{1\leq i \leq k }} \drond^{n_1} X \# \dots \# \drond^{n_k}X$$

    \begin{ex}<+->[Endofoncteur pour le $\lambda$-calcul linéaire]
      $$\Sigma X = \drond X + X \# X$$
    \end{ex}

  \item<+-> algèbre
    $$\Sigma X \to X$$

  \item<+-> tenseur de substitution
    $$ (X \otimes Y)(n) = \int^k X(k) \times G^{\# k}(n) $$

  \item<+-> monoïde
    $$ X \otimes X \to X $$

  \end{itemize}

\end{frame}
\begin{frame}

  \begin{itemize}

  \item<+-> $I$ neutre pour $\otimes$ : variables

  \item<+-> force
    $$\Sigma X \otimes Y \xto{\st} \Sigma (X \otimes Y)$$

  \item<+-> $\Z = (\Sigma + I)^\star\0$

  \item<+-> $\Sigma$-monoïde :
    \begin{center}
      \diag{
      \Sigma \Z \otimes \Z \& \Sigma (\Z \otimes \Z) \& \Sigma \Z \\
      \Z \otimes \Z        \&                        \& \Z
    }{
      (m-1-1) edge[labela={\st}] (m-1-2)
      edge (m-2-1)
      (m-1-2) edge (m-1-3)
      (m-1-3) edge (m-2-3)
      (m-2-1) edge (m-2-3)
    }
    \end{center}

    \begin{thm}<+->[Sémantique d'algèbre initiale]
      $\Z$ est le $\Sigma$-monoïde initial.
    \end{thm}

  \end{itemize}

\end{frame}

\subsection{Réduction}
\begin{frame}{Réduction}

  \begin{itemize}

  \item<+-> modules $$R \otimes \Z \to R $$
    $\to$ substitution

  \item<+-> règles : span $p(\Z^2) \from V \to \Z^2$

  \item<+-> modèles : dans la slice au dessus de $\Z^2$

  \item<+-> endofoncteur signature $\Sigma^r$ et algèbre pour les règles


    \begin{center}
      % TODO PULLBACK ?
      % \Diag{
      %   \pbk{m-1-1}{m-1-2}{m-2-2}
      % }{
      \diag{
        p(R)  \& \Sigma^r R \& R \\
        p(\Z^2) \& V          \& \Z^2
      }{
        (m-1-1) edge (m-2-1)
        (m-1-2) edge (m-1-1)
        edge (m-2-2)
        edge[dashed, labela={\phi}] (m-1-3)
        (m-2-2) edge (m-2-1)
        edge (m-2-3)
        (m-1-3) edge (m-2-3)
      }
    \end{center}

  \item<+-> $\substack{R \\ \downarrow \\ \Z^2}$ est un modèle des règles $\iff$ il existe un tel $\phi$
  \end{itemize}

\end{frame}
\begin{frame}{Quelle est la forme de $p$ ?}

  \visible<+->{$$p(X) = \prod_{i\in N} \pa{\drond^{\nu_i}X}\# 1 $$}

  \begin{itemize}
  \item<+-> $\prod_{i\in N}$ : $N$ prémisses
  \item<+-> $\drond^{\nu_i}$ : variables liées supplémentaires
  \item<+-> $-\# 1$ : permet << d'ignorer >> certaines variables
  \end{itemize}

  \begin{ex}<+->[Retour sur $App_g$]
    $\Z \in \PSet$

      \begin{center}
        \diag(0,1){
        (\Z^2 \only<+->{\# 1})n \& (\Z^2 \# \Z)n \& \Z^2n
        \only<+->{
          \\ = \int^{k,k'} \Z^2 k\times 1k'\times [k+k',n] \&\&
          \\ \cong \int^{k\leq n} \Z^2 k                \&\& }
      }{
        (m-1-2) edge (m-1-1)
        edge (m-1-3)
      }
      \end{center}
    \end{ex}

\end{frame}
\begin{frame}

  \begin{thm}<+->[Adámek]
    Soient $\C$ une catégorie cocomplète, $\0$ l'objet initial et $F$ un endofoncteur finitaire de $\C$,
    alors $U := F^\star \0$ est la $F$-algèbre initiale.
  \end{thm}

  \begin{itemize}
  \item<+-> $\Zmod/\Z^2$ cocomplète
  \item<+-> $\Sigma^r$  finitaire
  \end{itemize}

  \begin{thm}<+->[Sémantique d'algèbre initiale pour les règles]
    Soient $\Z$ un $\Sigma$-monoïde initial sur $\PSet$ et $\Sigma^r$ un endofoncteur signature des règles. Alors
    $$U = \pa{\Sigma^r} ^\star \pa{ \substack{\0 \\ \downarrow \\ \Z^2} }$$
    est la $\Sigma^r$-alèbre initiale sur $\Zmod/\Z^2$.
  \end{thm}

\end{frame}

\section{Conclusion}
\label{sec:ccl}

\begin{frame}{Conclusion}

  \begin{itemize}
  \item<+-> En résumé

    \begin{description}
    \item<+->[Connu] ~
      \begin{itemize}
      \item<+-> sémantique par algèbres initiales pour syntaxe linéaire

        signature liante linéaire $\to$ algèbre initiale
      \item<+-> sémantique par algèbres initiales pour syntaxe cartésienne + réduction

        signature liante cartésienne + règles $\to$ algèbre initiale
      \end{itemize}

    \item<+->[Contribution] ~
      \begin{itemize}
      \item<+-> adaptation de la notion de règle au cadre linéaire
      \item<+-> sémantique par algèbres initiales pour syntaxe et réduction linéaires

        signature liante + règles linéaires → algèbre initiale.
      \end{itemize}
    \end{itemize}

  \end{itemize}

\end{frame}
\begin{frame}

  \begin{itemize}

  \item<+-> Poursuite du travail
    \begin{itemize}
    \item<+-> linéaire-non-linéaire
    \item<+-> typage
    \end{itemize}

  \item<+-> Remerciements
    \begin{itemize}
    \item<+-> Tom et André
    \item<+-> l'équipe LIMD et Pierre
    \item<+-> Pacôme
    \item<+-> le LAMA, l'université Savoie-Mont-Blanc et le CROUS
    \end{itemize}
  \end{itemize}

\end{frame}

\begin{frame}[noframenumbering,plain]
\begin{center}
  \Huge Merci de votre attention. \\ Des questions ?
\end{center}
\end{frame}

\end{document}
